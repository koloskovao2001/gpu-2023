close all
clear all

max_system_log2 = 12;
min_system_log2 = 5;

cpu = [];
gpu = [];
gpu_shared = [];

for i_sys = min_system_log2 : 1 : max_system_log2
    i_sys
    [res, output] = system(strcat("gpu-2023 2 ", int2str(2^i_sys)));
    output = splitlines(output);
    cpu = [cpu; str2num(output{2})];
    gpu = [gpu; str2num(output{4})];
    gpu_shared = [gpu_shared; str2num(output{6})];
end

sys_ax = linspace(min_system_log2, max_system_log2, max_system_log2 - min_system_log2 + 1);
speedup_gpu = cpu ./ gpu;
speedup_gpu_shared = cpu ./ gpu_shared;
speedup_shared_class = gpu ./ gpu_shared;


f1 = figure();
semilogy(sys_ax, gpu, sys_ax, gpu_shared, sys_ax, cpu, 'LineWidth', 4);
legend("gpu", "gpu, shared memory", "cpu", 'Interpreter', 'latex')
xlabel("$$n = 2^x$$", 'Interpreter', 'latex')
title("$$T_{exe}(n), ms$$", 'Interpreter', 'latex')
f1.CurrentAxes.FontSize = 15;

f2 = figure();
plot(sys_ax, speedup_gpu, sys_ax, speedup_gpu_shared, 'LineWidth', 4);
legend("gpu", "gpu, shared memory", 'Interpreter', 'latex')
title("$$\frac{T_{gpu}}{T_{cpu}}$$", 'Interpreter', 'latex')
xlabel("$$n = 2^x$$", 'Interpreter', 'latex')
f2.CurrentAxes.FontSize = 15;

f3 = figure();
plot(sys_ax, speedup_shared_class, 'LineWidth', 4);
title("$$\frac{T_{shared}}{T_{gpu}}$$", 'Interpreter', 'latex')
xlabel("$$n = 2^x$$", 'Interpreter', 'latex')
f2.CurrentAxes.FontSize = 15;









