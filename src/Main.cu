#include <iostream>
#include <vector>
#include <random>
#include <string>
#include <Windows.h>

#include "CpuVector.h"
#include "GpuVector.h"
#include "CpuMatrix.h"
#include "GpuMatrix.h"

using namespace std;

void VectorTestCpu() {
    vector<int> shape1 = {3};
    double content1d[] = {1.23, 2.28, 3.32};
    CPUVector v1(shape1, content1d);
    content1d[0] = 0.77;
    content1d[1] = -0.28;
    content1d[2] = -1.32;
    std::cout << "vector a: ";
    v1.print();
    cout << "\n";
    CPUVector v2(shape1, content1d);
    std::cout << "vector b: ";
    v2.print();
    cout << "\n";
    v1 += v2;
    std::cout << "vector c=a+b: ";
    v1.print();
    cout << "\n";
    double content3d[] = {
        10,  -2, 4,
        9,   -3, 4,
        8,    -4,  -5,
        7,  -5,  -9,
        6,   -6,  -2,
        5, -7, 11};
    vector<int> shape2 = {2, 3, 3};
    CPUVector v3d(shape2, content3d);
    cout << "Vector d with shape (2, 3, 3): ";
    v3d.print();
    cout << "\n";
    cout << "d[1, 0, :]: ";
    auto slice = v3d[1][0];
    slice.print();
    cout << "\n";
    cout << "d[1, 0, :] + c: ";
    slice += v1;
    slice.print();
    cout << "\n";
    cout << "d: ";
    v3d.print();
    cout << "\n";
}   

void VectorTestGpu() {
    vector<int> shape1 = {3};
    double content1d[] = {1.23, 2.28, 3.32};
    GPUVector v1(shape1, content1d);
    content1d[0] = 0.77;
    content1d[1] = -0.28;
    content1d[2] = -1.32;
    std::cout << "vector a: ";
    v1.print();
    cout << "\n";
    GPUVector v2(shape1, content1d);
    std::cout << "vector b: ";
    v2.print();
    cout << "\n";
    v1 += v2;
    std::cout << "vector c=a+b: ";
    v1.print();
    cout << "\n";
    double content3d[] = {
        10,  -2, 4,
        9,   -3, 4,
        8,    -4,  -5,
        7,  -5,  -9,
        6,   -6,  -2,
        5, -7, 11};
    vector<int> shape2 = {2, 3, 3};
    GPUVector v3d(shape2, content3d);
    cout << "Vector d with shape (2, 3, 3): ";
    v3d.print();
    cout << "\n";
    cout << "d[1, 0, :]: ";
    auto slice = v3d[1][0];
    slice.print();
    cout << "\n";
    cout << "d[1, 0, :] + c: ";
    slice += v1;
    slice.print();
    cout << "\n";
    cout << "d: ";
    v3d.print();
    cout << "\n";
}

void MatrixTestCpu() {
    vector<int> shape = {3, 3};
    double A_data[] = {2, 1, 0, 1, -1, 2, 3, 2, 1};
    CPUMatrix A(shape[0], shape[1], A_data);
    double B_data[] = {1, 3, 1, 2, 0, 4, 1, 2, 3};
    CPUMatrix B(shape[0], shape[1], B_data);
    auto C = A * B;
    cout << "A matrix (3, 3): ";
    A.print();
    cout << "\n";
    cout << "B matrix (3, 3): ";
    B.print();
    cout << "\n";
    cout << "AxB matrix: ";
    C.print();
    cout << "\n";
}

void MatrixTestGpu() {
    vector<int> shape = {3, 3};
    double A_data[] = {2, 1, 0, 1, -1, 2, 3, 2, 1};
    GPUMatrix A(shape[0], shape[1], A_data);
    double B_data[] = {1, 3, 1, 2, 0, 4, 1, 2, 3};
    GPUMatrix B(shape[0], shape[1], B_data);
    auto C = A.mlp_classic(B);
    cout << "A matrix (3, 3): ";
    A.print();
    cout << "\n";
    cout << "B matrix (3, 3): ";
    B.print();
    cout << "\n";
    cout << "AxB matrix: ";
    C.print();
    cout << "\n";
    auto D = A.mlp_shared(B);
    cout << "AxB matrix with shared memory: ";
    D.print();
    cout << "\n";
}

void GetRandomArray(double* arr, int n) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(-5, 5);
    for (int i = 0; i < n; i++) {
        arr[i] = dis(gen);
    }
}

void Benchmark(int n) {
    int iterations = 10;
    vector<int> shape = { n, n };
    double* AContent = new double[shape[0] * shape[1]];
    double* BContent = new double[shape[0] * shape[1]];
    LARGE_INTEGER frequency;
    LARGE_INTEGER time[2];
    int64_t timeCpu = 0, timeGpu1 = 0, timeGpu2 = 0;
    QueryPerformanceFrequency(&frequency);
    for (int i = 0; i < iterations; i++) {
        GetRandomArray(AContent, shape[0] * shape[1]);
        GetRandomArray(BContent, shape[0] * shape[1]);
        CPUMatrix ACpu(shape[0], shape[1], AContent);
        CPUMatrix BCpu(shape[0], shape[1], BContent);
        QueryPerformanceCounter(&time[0]);
        CPUMatrix CCpu = ACpu * BCpu;
        QueryPerformanceCounter(&time[1]);
        timeCpu += time[1].QuadPart - time[0].QuadPart;
        GPUMatrix AGpu1(shape[0], shape[1], AContent);
        GPUMatrix BGpu1(shape[0], shape[1], BContent);
        QueryPerformanceCounter(&time[0]);
        GPUMatrix CGpu1 = AGpu1.mlp_classic(BGpu1);
        QueryPerformanceCounter(&time[1]);
        timeGpu1 += time[1].QuadPart - time[0].QuadPart;
        GPUMatrix AGpu2(shape[0], shape[1], AContent);
        GPUMatrix BGpu2(shape[0], shape[1], BContent);
        QueryPerformanceCounter(&time[0]);
        GPUMatrix CGpu2 = AGpu2.mlp_shared(BGpu2);
        QueryPerformanceCounter(&time[1]);
        timeGpu2 += time[1].QuadPart - time[0].QuadPart;
    }
    delete[] AContent;
    delete[] BContent;
    cout << "Cpu time, ms: " << endl << ((double)timeCpu / iterations) * 1000.0 / frequency.QuadPart << endl;
    cout << "Gpu classic time:, ms: " << endl << ((double)timeGpu1 / iterations) * 1000.0 / frequency.QuadPart << endl;
    cout << "Gpu shared time:, ms: " << endl << ((double)timeGpu2 / iterations) * 1000.0 / frequency.QuadPart << endl;
}


#define FUNCTIONAL_TEST 1
#define TIME_TEST 2

int main(int argc, char* argv[]) {
    switch (std::stoi(argv[1])) {
    case FUNCTIONAL_TEST:
        std::cout << "CPU vector:\n";
        VectorTestCpu();
        std::cout << "\nGPU vector:\n";
        VectorTestGpu();
        std::cout << "\nCPU matrix:\n";
        MatrixTestCpu();
        std::cout << "\nGPU matrix:\n";
        MatrixTestGpu();
        break;
    case TIME_TEST:
        Benchmark(std::stoi(argv[2]));
    }
    return 0;
}
