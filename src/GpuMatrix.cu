#include <device_launch_parameters.h>

#include "GpuMatrix.h"
#include "cuda_runtime.h"

#define BLOCK_SIZE 32

GPUMatrix::GPUMatrix(int n, int m, double* data) : GPUVector({n, m}, data) {}

__host__ __device__ inline int CeilDiv(int x, int y) { return 1 + (x - 1) / y; }

__global__ void MlpKernel(double* A, int an, int am, double* B, int bm, double* C) {
    int i = blockDim.y * blockIdx.y + threadIdx.y;
    int j = blockDim.x * blockIdx.x + threadIdx.x;

    double sum = 0;

    if (i < an && j < bm) {
        for (int k = 0; k < am; k++) {
            sum += A[i * am + k] * B[k * bm + j];
        }
        C[i * bm + j] = sum;
    }
}

__global__ void MlpKernelShared(double* A, int an, int am, double* B, int bm, double* C) {
    __shared__ double sA[BLOCK_SIZE][BLOCK_SIZE];
    __shared__ double sB[BLOCK_SIZE][BLOCK_SIZE];

    int i = blockDim.y * blockIdx.y + threadIdx.y;
    int j = blockDim.x * blockIdx.x + threadIdx.x;
    double sum = 0;

    if (i >= an || j >= bm) {
        return;
    }

    for (int k = 0; k < CeilDiv(am, BLOCK_SIZE); k++) {
        int local_j = (int)threadIdx.x + k * BLOCK_SIZE;
        if (i < an && local_j < am) {
            sA[threadIdx.y][threadIdx.x] = A[i * am + local_j];
        }
        else {
            sA[threadIdx.y][threadIdx.x] = 0;
        }
        int local_i = (int)threadIdx.y + k * BLOCK_SIZE;
        if (j < bm && local_i < am) {
            sB[threadIdx.y][threadIdx.x] = B[local_i * bm + j];
        }
        else {
            sB[threadIdx.y][threadIdx.x] = 0;
        }
        __syncthreads();

        for (int l = 0; l < BLOCK_SIZE; l++) {
            sum += sA[threadIdx.y][l] * sB[l][threadIdx.x];
        }
        __syncthreads();
    }
    C[i * bm + j] = sum;
}

GPUMatrix GPUMatrix::mlp_shared(GPUMatrix& other) {
    if (shape[1] != other.shape[0]) {
        return *this;
    }
    GPUMatrix result(shape[0], other.shape[1], nullptr);
    dim3 blocks(CeilDiv(other.shape[1], BLOCK_SIZE), CeilDiv(other.shape[1], BLOCK_SIZE), 1);
    dim3 threads(BLOCK_SIZE, BLOCK_SIZE, 1);
    MlpKernelShared<<<blocks, threads>>>(getPointer(), shape[0], shape[1], other.getPointer(),
                                         other.shape[1], result.getPointer());
    cudaDeviceSynchronize();
    return result;
}

GPUMatrix GPUMatrix::mlp_classic(GPUMatrix& other) {
    if (shape[1] != other.shape[0]) {
        return *this;
    }
    GPUMatrix result(shape[0], other.shape[1], nullptr);
    dim3 blocks(CeilDiv(other.shape[1], BLOCK_SIZE), CeilDiv(other.shape[1], BLOCK_SIZE), 1);
    dim3 threads(BLOCK_SIZE, BLOCK_SIZE, 1);
    MlpKernel<<<blocks, threads>>>(getPointer(), shape[0], shape[1], other.getPointer(),
                                   other.shape[1], result.getPointer());
    cudaDeviceSynchronize();
    return result;
}
