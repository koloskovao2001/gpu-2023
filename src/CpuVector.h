#pragma once
#include <vector>
#include <memory>

using namespace std;

class CPUVector {
protected:
    vector<int> shape;
    shared_ptr<double[]> data;
    int sliceShift = 0;
    CPUVector(vector<int>& shape, int subIndex, int prevShift, shared_ptr<double[]> data);
    double* getPointer();
public:
    CPUVector(vector<int>& shape, double* data);
    CPUVector(vector<int>&& shape, double* data);
    CPUVector& operator+=(CPUVector& other);
    CPUVector operator[](int i);
    int getElemsCount();
    void fill(double* data);
    void print();
    ~CPUVector();
};