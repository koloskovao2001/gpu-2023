#pragma once
#include "GpuVector.h"

class GPUMatrix: public GPUVector {
public:
    GPUMatrix(int n, int m, double* data);
    GPUMatrix mlp_shared(GPUMatrix& other);
    GPUMatrix mlp_classic(GPUMatrix& other);
    ~GPUMatrix() = default;
};