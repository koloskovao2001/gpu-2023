#pragma once
#include "CpuVector.h"

class CPUMatrix: public CPUVector {
public:
    CPUMatrix(int n, int m, double* data);
    CPUMatrix operator *(CPUMatrix& other);
    ~CPUMatrix() = default;
};