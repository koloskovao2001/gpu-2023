#include "CpuMatrix.h"

CPUMatrix::CPUMatrix(int n, int m, double* data): CPUVector({n, m}, data) {

}

CPUMatrix CPUMatrix::operator*(CPUMatrix& other) {
    if (shape[1] != other.shape[0]) {
        return *this;
    }
    CPUMatrix result(shape[0], other.shape[1], nullptr);
    double* res_ptr = result.getPointer();
    double* ptrA = getPointer();
    double* ptrB = other.getPointer();
    for (int i = 0; i < shape[0]; i++) {
        for (int j = 0; j < other.shape[1]; j++) {
            res_ptr[i * other.shape[1] + j] = 0;
            for (int k = 0; k < shape[1]; k++) {
                res_ptr[i * other.shape[1] + j] += ptrA[i * shape[1] + k] * ptrB[j + k * other.shape[1]];
            }
        }
    }
    return result;
}
