#pragma once
#include <vector>
#include <memory>

using namespace std;

class GPUVector {
protected:
    vector<int> shape;
    shared_ptr<double> data;
    int sliceShift = 0;
    GPUVector(vector<int>& shape, int subIndex, int prevShift, shared_ptr<double> data);
    double* getPointer();
public:
    GPUVector(vector<int>& shape, double* data);
    GPUVector(vector<int>&& shape, double* data);
    GPUVector& operator+=(GPUVector& other);
    GPUVector operator[](int i);
    int getElemsCount();
    void fill(double* data);
    void print();
    ~GPUVector();
};