#include <iostream>
#include <device_launch_parameters.h>
#include "cuda_runtime.h"
#include "CpuVector.h"
#include "GpuVector.h"

class CudaDeleter {
public:
    void operator()(double* ptr) const {
        cudaFree((void*)ptr);
    }
};

int GPUVector::getElemsCount() {
    int n = 1;
    for (int s: shape) {
        n *= s;
    }
    return n;
}

double* GPUVector::getPointer() { 
    return data.get() + sliceShift;
}

GPUVector::GPUVector(vector<int>& shape, double* data) : shape(shape) {
    int N = getElemsCount();
    double* ptr;
    cudaError_t rc = cudaMalloc((void**)&ptr, N * sizeof(double));
    if (rc != cudaSuccess) {
        cout << "Cuda malloc error" << endl;
    }
    this->data = shared_ptr<double>(ptr, CudaDeleter());
    if (data) {
        cudaMemcpy(ptr, data, N * sizeof(double), cudaMemcpyHostToDevice);
    }
}

GPUVector::GPUVector(vector<int>&& shape, double* data) : shape(shape) {
    int N = getElemsCount();
    double* ptr;
    cudaError_t rc = cudaMalloc((void**)&ptr, N * sizeof(double));
    if (rc != cudaSuccess) {
        cout << "Cuda malloc error" << endl;
    }
    this->data = shared_ptr<double>(ptr, CudaDeleter());
    if (data) {
        cudaMemcpy(ptr, data, N * sizeof(double), cudaMemcpyHostToDevice);
    }
}

__host__ __device__ inline int CeilDiv(int x, int y) {
    return 1 + (x - 1) / y;
}


__global__ void SumKernel(double* a, double* b, int n) {
    int idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx < n) {
        a[idx] += b[idx];
    }
}

GPUVector& GPUVector::operator+=(GPUVector& other) {
    int N = getElemsCount();
    dim3 blocks(CeilDiv(N, 1024), 1, 1);
    dim3 threads(min(N, 1024), 1, 1);
    SumKernel<<<blocks, threads>>>(getPointer(), other.getPointer(), N);
    cudaDeviceSynchronize();
    return *this;
}

GPUVector::GPUVector(vector<int>& shape, int subIndex, int prevShift, shared_ptr<double> data) {
    int singleShift = 1;
    for (int i = 1; i < shape.size(); i++) {
        this->shape.push_back(shape[i]);
        singleShift *= shape[i];
    }
    sliceShift = prevShift + subIndex * singleShift;
    this->data = data;
}

GPUVector GPUVector::operator[](int i) { 
    return GPUVector(shape, i, sliceShift, data); 
}

void GPUVector::print() {
    int N = getElemsCount();
    double* buffer = new double[N];
    cudaMemcpy(buffer, getPointer(), N * sizeof(double), cudaMemcpyDeviceToHost);
    for (int i = 0; i < N; i++) {
        cout << buffer[i] << ' ';
    }
    delete[] buffer;
}

GPUVector::~GPUVector() { 
}

void GPUVector::fill(double* data) {
    int N = getElemsCount();
    cudaMemcpy(getPointer(), data, N * sizeof(double), cudaMemcpyHostToDevice);
}
