#include <iostream>
#include "CpuVector.h"

int CPUVector::getElemsCount() {
    int n = 1;
    for (int s: shape) {
        n *= s;
    }
    return n;
}

CPUVector::CPUVector(vector<int>& shape, int subIndex, int prevShift, shared_ptr<double[]> data) {
    int singleShift = 1;
    for (int i = 1; i < shape.size(); i++) {
        this->shape.push_back(shape[i]);
        singleShift *= shape[i];
    }
    sliceShift = prevShift + subIndex * singleShift;
    this->data = data;
}

double* CPUVector::getPointer() { 
    return data.get() + sliceShift;
}

CPUVector::CPUVector(vector<int>& shape, double* data): shape(shape) {
    int N = getElemsCount();
    this->data = make_shared<double[]>(N);
    if (data) {
        for (int i = 0; i < N; i++) {
            this->data[i] = data[i];
        }
    }
}

CPUVector::CPUVector(vector<int>&& shape, double* data) : shape(shape) {
    int N = getElemsCount();
    this->data = make_shared<double[]>(N);
    if (data) {
        for (int i = 0; i < N; i++) {
            this->data[i] = data[i];
        }
    }
}

CPUVector& CPUVector::operator+=(CPUVector& other) {
    int N = getElemsCount();
    double* ptr = getPointer();
    for (int i = 0; i < N; i++) {
        ptr[i] += other.data[i];
    }
    return *this;
}

void CPUVector::print() {
    int N = getElemsCount();
    double* ptr = getPointer();
    for (int i = 0; i < N; i++) {
        cout << ptr[i] << ' ';
    }
}

CPUVector::~CPUVector() {
}

CPUVector CPUVector::operator[](int i) { 
    return CPUVector(shape, i, sliceShift, data);
}

void CPUVector::fill(double* data) {
    int N = getElemsCount();
    double* ptr = getPointer();
    for (int i = 0; i < N; i++) {
        ptr[i] = data[i];
    }
}
